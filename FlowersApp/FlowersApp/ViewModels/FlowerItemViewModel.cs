﻿using System;
using FlowersApp.Models;
using GalaSoft.MvvmLight.Command;
using FlowersApp.Services;
using System.Windows.Input;

namespace FlowersApp.ViewModels
{
    public class FlowerItemViewModel : Flower
    {
        private NavigationService navigationService;

        public FlowerItemViewModel()
        {
            navigationService = new NavigationService();
        }

        public ICommand EditFlowerCommand { get { return new RelayCommand(EditFlower); } }

        private async void EditFlower()
        {
            await navigationService.EditFlower(this);
        }
    }
}
