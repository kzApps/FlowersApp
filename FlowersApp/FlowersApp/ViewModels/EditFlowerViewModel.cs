﻿using FlowersApp.Models;
using FlowersApp.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FlowersApp.ViewModels
{
    public class EditFlowerViewModel : Flower, INotifyPropertyChanged
    {
        #region Attributes
        private DialogService dialogService;
        private ApiService apiService;
        private NavigationService navigationService;
        private bool isRunning;
        private bool isEnabled;
        #endregion

        #region Properties
        public bool IsRunning
        {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRunning"));
                }
            }
            get
            {
                return isRunning;
            }
        }

        public bool IsEnabled
        {
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsEnabled"));
                }
            }
            get
            {
                return isEnabled;
            }
        }
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Constructors
        public EditFlowerViewModel(Flower flower)
        {
            dialogService = new DialogService();
            apiService = new ApiService();
            navigationService = new NavigationService();

            Description = flower.Description;
            Price = flower.Price;
            FlowerId = flower.FlowerId;

            IsEnabled = true;
            IsRunning = false;

        }
        #endregion

        #region Commands

        public ICommand SaveFlowerCommand { get { return new RelayCommand(SaveFlower); } }

        private async void SaveFlower()
        {
            if (string.IsNullOrEmpty(Description))
            {
                await dialogService.ShowMessage("Error", "You must enter a description");
                return;
            }

            if (Price <= 0)
            {
                await dialogService.ShowMessage("Error", "You must enter a number greater than zero in price");
                return;
            }

            IsRunning = true;
            IsEnabled = false;

            var response = await apiService.Put("http://flowersbackkz.azurewebsites.net", "/api", "/Flowers", this);

            IsRunning = false;
            IsEnabled = true;

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage("Error", response.Message);
                return;
            }

            await navigationService.Back();
        }

        public ICommand DeleteFlowerCommand { get { return new RelayCommand(DeleteFlower); } }

        private async void DeleteFlower()
        {
            var answer = await dialogService.ShowConfirm("Confirm","Are you sure to delete this?");
            if (!answer)
            {
                return;
            }

            IsRunning = true;
            IsEnabled = false;

            var response = await apiService.Delete("http://flowersbackkz.azurewebsites.net", "/api", "/Flowers", this);

            IsRunning = false;
            IsEnabled = true;

            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage("Error", response.Message);
                return;
            }

            await navigationService.Back();
        }

        #endregion


    }
}
