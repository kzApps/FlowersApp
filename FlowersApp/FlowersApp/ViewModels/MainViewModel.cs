﻿using FlowersApp.Models;
using FlowersApp.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FlowersApp.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region Attributes
        private ApiService apiService;
        private NavigationService navigationService;
        private DialogService dialogService;
        private bool isRefreshing;
        #endregion

        #region Properties
        public ObservableCollection<FlowerItemViewModel> Flowers { get; set; }

        public NewFlowerViewModel NewFlower { get; set; }

        public EditFlowerViewModel EditFlower { get; set; }

        public bool IsRefreshing
        {
            set
            {
                if(isRefreshing != value)
                {
                    isRefreshing = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRefreshing"));
                }
            }
            get
            {
                return isRefreshing;
            }
        }
        #endregion

        #region Constructors
        public MainViewModel()
        {
            // Singleton
            instance = this;

            // Services
            apiService = new ApiService();
            navigationService = new NavigationService();
            dialogService = new DialogService();

            // ViewModels
            Flowers = new ObservableCollection<FlowerItemViewModel>();
            
        }
        #endregion

        #region Methods
        private async Task LoadFlowers()
        {
            var response = await apiService.Get<Flower>("http://flowersbackkz.azurewebsites.net", "/api", "/Flowers");
            if (!response.IsSuccess)
            {
                await dialogService.ShowMessage("Error", response.Message);
                return;
            }
            ReloadFlowers((List<Flower>)response.Result);
        }

        private void ReloadFlowers(List<Flower> flowers)
        {
            Flowers.Clear();
            foreach (var flower in flowers.OrderBy(f => f.Description))
            {
                Flowers.Add(new FlowerItemViewModel
                {
                    Description = flower.Description,
                    FlowerId = flower.FlowerId,
                    Price = flower.Price,
                });
            }
        }
        #endregion

        #region Commands
        public ICommand RefreshFlowersCommand { get { return new RelayCommand(RefreshFlowers); } }

        private async void RefreshFlowers()
        {
            IsRefreshing = true;
            LoadFlowers();
            IsRefreshing = false;
        }

        public ICommand AddFlowerCommand { get { return new RelayCommand(AddFlower); } }

        private async void AddFlower()
        {
            await navigationService.Navigate("NewFlowerPage");
        }


        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Singleton

        private static MainViewModel instance;


        public static MainViewModel GetInstance()
        {
            if(instance == null)
            {
                instance = new MainViewModel();
            }

            return instance;
        }

        #endregion
    }
}
