﻿using FlowersApp.Models;
using FlowersApp.Pages;
using FlowersApp.ViewModels;
using System.Threading.Tasks;

namespace FlowersApp.Services
{
    public class NavigationService
    {
        public async Task Navigate(string pageName)
        {
            var mainViewModel = MainViewModel.GetInstance();
            switch (pageName)
            {
                case "NewFlowerPage":
                    mainViewModel.NewFlower = new NewFlowerViewModel();
                    await App.Current.MainPage.Navigation.PushAsync(new NewFlowerPage());
                    break;
                default:
                    break;
            }
        }

        public async Task EditFlower(Flower flower)
        {
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.EditFlower = new EditFlowerViewModel(flower);
            await App.Current.MainPage.Navigation.PushAsync(new EditFlowerPage());
        }

        public async Task Back()
        {
            await App.Current.MainPage.Navigation.PopAsync();
        }
    }
}
